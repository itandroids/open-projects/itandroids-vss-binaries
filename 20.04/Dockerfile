FROM ubuntu:20.04

# Primary packages
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install --no-install-recommends -y sudo \
    git \
    wget \
    cmake \
    unzip \
    build-essential \
    libboost-all-dev \
    libeigen3-dev \
    qt5-default \
    libgtk2.0-dev \
    pkg-config \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libois-dev \
    libraw1394-11 \
    libusb-1.0-0 \
    libogre-1.9-dev ogre-1.9-dev \
    ogre-1.9-tools \
    libusb-1.0-0 \
    libzmq3-dev \
    libglade2-0 \
    libglademm-2.4-1v5 \
    libgtkmm-2.4-1v5 \
    libatkmm-1.6-dev \
    libcairomm-1.0-dev \
    libglade2-dev \
    libglademm-2.4-dev \
    libglibmm-2.4-dev \
    libgtkglext1-dev \
    libgtkglextmm-x11-1.2-0v5 \
    libgtkglextmm-x11-1.2-dev \
    libgtkmm-2.4-dev \
    libpangomm-1.4-dev \
    libpangox-1.0-dev \
    libsigc++-2.0-dev \
    libxml2 \
    libxml2-dev \
    libxmu-dev \
    libxmu-headers \
    libxt-dev \
    libavcodec58 libavformat58 libswscale5 libswresample3 \
    libavutil56 libusb-1.0-0 libpcre2-16-0 \
    libdouble-conversion3 libxcb-xinput0 libxcb-xinerama0 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ODE Package
RUN wget https://bitbucket.org/odedevs/ode/downloads/ode-0.16.2.tar.gz --no-check-certificate -O ode-0.16.2.tar.gz \
    && tar -zxvf ode-0.16.2.tar.gz && rm ode-0.16.2.tar.gz

WORKDIR /ode-0.16.2/

# Make build ODE
RUN ./configure --enable-double-precision --with-demos CXXFLAGS="-fpic" \
    && make -j4 && make install

WORKDIR /

# Protobuf Package
RUN wget https://github.com/google/protobuf/releases/download/v3.5.1/protobuf-cpp-3.5.1.tar.gz --no-check-certificate \
    && tar -xvf protobuf-cpp-3.5.1.tar.gz && rm protobuf-cpp-3.5.1.tar.gz 
    
WORKDIR /protobuf-3.5.1/

# Make build protobuf
RUN ./configure && make -j 4 \
    && make install \
    && ldconfig

WORKDIR /

# Kdtree package
RUN git config --global http.sslVerify false && sudo git clone https://github.com/jtsiomb/kdtree.git

WORKDIR /kdtree/

# Make build kdtree
RUN ./configure && make -j 4 && make install && ldconfig

# Copying configs, resources, binaries and externals
COPY resources/ /resources/
RUN ls -la /resources/*

COPY configs/ /configs/
RUN ls -la /configs/*

COPY binaries/ /binaries/
RUN ls -la /binaries/*

COPY externals/ /externals/
RUN ls -la /binaries/*

WORKDIR /externals/flycapture/

# Flycapture Packages
RUN sudo dpkg -i libflycapture-2* \
    && sudo dpkg -i libflycapturegui-2* \
    && sudo dpkg -i libflycapture-c-2* \
    && sudo dpkg -i libflycapturegui-c-2* \
    && sudo dpkg -i libmultisync-2* \
    && sudo dpkg -i libmultisync-c-2* \
    && sudo dpkg -i flycap-2* \
    && sudo dpkg -i flycapture-doc-2* \
    && sudo dpkg -i updatorgui*

# Spinnaker Packages
WORKDIR /externals/spinnaker/20.04/

RUN echo libgentl libspinnaker/accepted-flir-eula boolean true | sudo debconf-set-selections

RUN sudo dpkg -i libgentl_*.deb \
    && sudo dpkg -i libspinnaker_*.deb \
    && sudo dpkg -i libspinnaker-dev_*.deb \
    && sudo dpkg -i libspinnaker-c_*.deb \
    && sudo dpkg -i libspinnaker-c-dev_*.deb \
    && sudo dpkg -i libspinvideo_*.deb \
    && sudo dpkg -i libspinvideo-dev_*.deb \
    && sudo dpkg -i libspinvideo-c_*.deb \
    && sudo dpkg -i libspinvideo-c-dev_*.deb \
    && sudo apt-get install -y ./spinview-qt_*.deb \
    && sudo dpkg -i spinview-qt-dev_*.deb \
    && sudo dpkg -i spinupdate_*.deb \
    && sudo dpkg -i spinupdate-dev_*.deb \
    && sudo dpkg -i spinnaker_*.deb \
    && sudo dpkg -i spinnaker-doc_*.deb

WORKDIR /binaries/

# Run the specified command within the container.
CMD ./agent -1s -noProj -f -noGUI
